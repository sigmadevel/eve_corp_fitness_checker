Installation:

Clone the repository
cd into the top level
pip install -r requirements.txt

Usage Instructions:

cd into the src directory under the top level
fill in api.csv with any api keys you want checked
add your fits to the fits sub dir (EFT or Pyfa format)
customise the CHECKED_CORPS variable in corp_fitness_test.py
run corp_fitness_test.py

