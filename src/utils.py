import itertools



def get_skills(sdi):
    reqs = []
    for attr in ("item", "charge"):
        item = getattr(sdi, attr, None)
        if item is not None:
            for skill, level in item.requiredSkills.iteritems():
                reqs.append((skill, level))
                reqs.extend(recurse_get_skills(skill))
    return reqs


def recurse_get_skills(skill):
    reqs = []
    for sk, level in skill.requiredSkills.iteritems():
        reqs.append((sk, level))
        reqs.extend(recurse_get_skills(sk))
    return reqs


def compute_reqs(fit):
    req_list = []
    for sdi in itertools.chain(fit.modules, fit.drones, (fit.ship,)):
        req_list.extend(get_skills(sdi))
    req_dict = {}
    for req in req_list:
        if req[0] in req_dict:
            if req_dict[req[0]] < req[1]:
                req_dict[req[0]] = req[1]
        else:
            req_dict[req[0]] = req[1]
    return req_dict


def find_unmet_skills(char, fit):
    reqs = compute_reqs(fit)
    unmet_skills = [(skill.name, level, char.getSkill(skill).level)
                    for skill, level in reqs.iteritems()
                    if char.getSkill(skill).level < level]
    return unmet_skills
