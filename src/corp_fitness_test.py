import os
import locale
import csv

import eos.eveapi
import eos.config
eos.config.gamedata_connectionstring = ''.join(['sqlite:///',
                                                os.getcwd(),
                                                '/staticdata/eve.db'])
import eos.db
from utils import find_unmet_skills

CHECKED_CORPS = ["Wildly Inappropriate", ]

defencode = locale.getpreferredencoding()

#read in all eft fits in a fits subdir
fits = []
for root, dirs, files in os.walk("fits"):
    for name in files:
        with open(os.path.join(root, name)) as fit_file:
            eft_str = unicode(fit_file.read(), defencode)
            fits.extend([fit for fit in eos.db.fit.Fit.importAuto(eft_str)[1]])

#read in api entries from api.csv
#csv should be formatted so that each line looks like:
#api_id,api_key
#ensure there are no trailing newlines in the csv, this _will_ blow up
api_entries = []
with open("api.csv") as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        api_entries.append(row)

#read api info on characters
for api_entry in api_entries:
    api = eos.eveapi.EVEAPIConnection()
    auth = api.auth(keyID=int(api_entry[0]), vCode=api_entry[1])
    for toon in auth.account.Characters().characters:
        #check if the toon is in a corp you care about and not a random alt
        if toon.corporationName not in CHECKED_CORPS:
            continue
        new_char = eos.types.Character(toon.name)
        new_char.apiID = api_entry[0]
        new_char.apiKey = api_entry[1]
        new_char.apiFetch(toon.name)

        eos.db.save(new_char)

for char in eos.db.getCharacterList():
    for fit in fits:
        print(find_unmet_skills(char, fit))
